<?php

use App\Http\Controllers\PublicController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class,'home'])->name('home');

Route::get('/chi-siamo', [PublicController::class,'team'])->name('chi-siamo');

Route::get('/servizi', [PublicController::class,'service'])->name('servizi');

Route::get('/contattaci', [PublicController::class,'contact'])->name('contattaci');

Route::post('/contattaci/submit', [PublicController::class,'submit'])->name('contattaci.submit');

Route::get('/grazie', [PublicController::class,'grazie'])->name('grazie');

Route::get('/servizi/dettaglio/{title}', [PublicController::class,'dettaglio'])->name('servizi.dettaglio');
