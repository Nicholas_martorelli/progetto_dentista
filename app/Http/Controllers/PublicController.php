<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PublicController extends Controller
{
    public function home () {
        return view('home');
    }

    public function contact () {
        return view('contattaci');
    }

    public function team () {

        $cards =[
            ['img'=>'/img/dentista-4.png','name'=>'Elisa', 'surname'=>'Verdi','description'=>'Laureata a pieni voti in odontoiatria e protesi dentaria presso l’università degli studi di Bari nel 2012. Specializzata in ortognatodonzia presso l’università degli studi di Torino nel 2016.Dal 2012 frequenta numerosi corsi di specializzazione internazionali nelle varie branche dell’ortognatodonzia.'],


            ['img'=>'/img/dentista-3.jpeg','name'=>'Giorgio', 'surname'=>'Sole','description'=>'Laureato a pieni voti in odontoiatria e protesi dentaria presso l’università degli studi di Torino nel 2010.
             Dal 2017 frequenta il corso di specializzazione in ortognatodonzia dell’università degli studi di Torino. Dal 2010 ad oggi frequenta numerosi corsi di specializzazione in conservativa, endodonzia e gnatologia.'],

             
            ['img'=>'/img/dentista-2.jpg','name'=>'Matteo', 'surname'=>' Gallo','description'=>'Diplomato odontotecnico presso I.P.S.I.A. G.PLANA di Torino nel 1988
            Si forma come odontotecnico e consegue specializzazioni in protesi fissa e mobile presso importanti laboratori odontotecnici in Torino fino al 1998.
            Laureato a pieni voti in “Odontoiatria e Protesi Dentaria” presso l’università degli studi di Torino nel 2002. Dallo stesso anno sviluppa consulenze di protesi e chirurgia implantare presso importanti studi odontoiatrici nel torinese.'],


            ['img'=>'/img/dentista-1.jpg','name'=>'Lucia', 'surname'=>'Rossi','description'=>'Dal 2004 svolge la mansione di assistente alla poltrona e della gestione dello studio odontoiatrico acquisendo una notevole esperienza in questo settore e seguendo numerosi corsi per tenersi sempre aggiornata.']
        ];
        return view('chi-siamo',['cards'=>$cards]);
    }

    public function service () {

        $cards =[
            ['img'=>'/img/Ortodonzia.jpg','title'=>'Ortodonzia'],
            ['img'=>'/img/Chirurgia odontostomatologica.jpg','title'=>'Chirurgia odontostomatologica'],
            ['img'=>'/img/Implantologia.jpg','title'=>'Implantologia'],
            ['img'=>'/img/Protesi dentaria.jpg','title'=>'Protesi dentaria'],
            ['img'=>'/img/Odontoiatria infantile.jpg','title'=>'Odontoiatria infantile'],
            ['img'=>'/img/Sedazione Cosciente.jpg','title'=>'Sedazione Cosciente']
        ];
        return view('servizi',['cards'=>$cards]);
    }

    public function dettaglio($title){

        $cards = [
            ['img'=>'/img/Ortodonzia.jpg','title'=>'Ortodonzia','description'=>'Chi è l’ortodontista, o meglio, l’ortognatodontista?
            Gli ortodontisti, inizialmente, sono dentisti, ma non tutti i dentisti diventano ortognatodontisti. È importante che il trattamento ortodontico venga eseguito da professionisti competenti poiché modifica in modo permanente la posizione dei denti, dei competenti mascellari e il profilo del viso.Chi può praticare l’ortodonzia?
            Possono praticare ortodonzia gli abilitati all’esercizio dell’odontoiatria, e quindi tutti i laureati in odontoiatria o i medici chirurghi inseriti nell’albo degli odontoiatri.
            Alcuni di questi laureati (più di 3.000) sono soci S.I.D.O. (Società Italiana di Ortodonzia). Gli iscritti a questa società scientifica sono dei cultori di questa disciplina, e vengono costantemente aggiornati sulle nuove tecniche e metodologie.','indicazioni'=>'La visita rappresenta il momento irrinunciabile per poter fare una diagnosi ortodontica: Se la valutazione clinica non ravvisa alcun tipo di anomalia l’ortodontista può decidere di rivedere il bambino negli anni successivi per verificare nel tempo il corretto sviluppo della dentatura e l’entità e direzione di crescita delle strutture scheletriche del complesso cranio-facciale. In alcuni casi può richiedere ugualmente una radiografia delle arcate dentarie (ortopantomografia) per verificare presenza e posizionamento degli elementi permanenti non ancora erotti. In questa occasione l’ortognatodontista deve comunque fornire utili consigli per l’igiene orale e la prevenzione delle affezioni del cavo orale.'],


            ['img'=>'/img/Chirurgia odontostomatologica.jpg','title'=>'Chirurgia odontostomatologica','description'=>'La chirurgia odontostomatologica si occupa delle estrazione di denti molto malati o distrutti che non è possibile salvare con altra terapia (per es con la endodonzia), estrazione di residui radicolari; rimozione di radici o di denti rimasti inclusi o semi-inclusi nell’osso come gli “ottavi in disodontiasi”; asportazione dell’apice del dente coinvolto in processi infiammatori quando non trattabili con la sola endodonzia, asportazione di cisti e piccoli tumori e neoformazioni del cavo orale.', 'indicazioni'=>'Inoltre piccoli e grandi rialzi del seno mascellare, per aumentare lo spessore dell’osso in sedi implantari, eseguiti con inserimento di osso sintetico (bio-oss) o autologo ( prelevato dalla sinfisi mentoniera del paziente).'],



            ['img'=>'/img/Implantologia.jpg','title'=>'Implantologia','description'=>'Sostituzione di elementi dentali compromessi o già mancanti, con impianti in titanio attraverso un intervento chirurgico. A guarigione avvenuta (processo di “osteointegrazione” dell’impianto) èpossibile alloggiare nell’impianto (invisibile in bocca) un perno-moncone (visibile in bocca), quest’ultimo viene utilizzato come appoggio di protesi fissa o anche di protesi rimovibile (contribuendo alla sua stabilità e ancoraggio).','indicazioni'=>'Dalla TC preoperatoria all’analisi delle migliori metodiche in fase chirurgica: qualsiasi passo che porta all’inserimento dell’impianto viene valutato attentamente in maniera digitale. Questo ha avuto notevoli risvolti positivi: la pianificazione accurata consente di minimizzare i traumi dovuti all’intervento e ciò si riflette in una più semplice gestione del paziente e del post-intervento.'],




            ['img'=>'/img/Protesi dentaria.jpg','title'=>'Protesi dentaria','description'=>'Si occupa di sostituire elementi dentali mancanti con manufatti più o meno estesi. Comprende: una Protesi fissa su elementi dentari naturali e/o artificiali (v. impianti e implantologia); una protesi rimovibile parziale (quando rimpiazza solo una parte di denti assenti) o totale (quando rimpiazza una totale assenza di denti su una arcata o su entrambe, superiore ed inferiore)','indicazioni'=>"Molti portatori di protesi dentarie all'inizio hanno provato una vera e propria paura di sorridere. Erano ansiosi che la loro protesi dentaria potesse scivolare quando meno se lo apettavano. Grazie alle creme adesive puoi riscoprire un bel sorriso, ma anche la fiducia necessaria per rilassarsi, essere spontaneo e godersi la vita."],




            ['img'=>'/img/Odontoiatria infantile.jpg','title'=>'Odontoiatria infantile','description'=>'Nell’ambito del rispetto delle indicazioni del ministero della salute viene applicato il programma PPS con lo scopo di prevenire le carie, le gengiviti, le patologie delle mucose orali e le malocclusioni dalla nascita alla adolescenza.
            IL CENTRO APPLICA NELLA PRATICA QUOTIDIANA LE RACCOMANDAZIONI PER LA PROMOZIONE DELLA SALUTE ORALE E LA PREVENZIONE DELLE PATOLOGIE ORALI IN ETA’ EVOLUTIVA DEL MINISTERO DELLA SALUTE.','indicazioni'=>'I denti da latte sono in primo dei naturali mantenitori di spazi per i denti permanenti. E’ quindi fondamentale preparare correttamente questo processo fisiologico. La cura dei denti da latte è inoltre necessaria per evitare che piccole carie progrediscano e diventino ascessi continui e dolorosi. La cura è quindi altamente raccomandata, tenendo conto che una precoce perdita dei denti da latte può determinare gravi malposizioni dentali nella dentatura permanente.Lo studio pratica espressamente la pedodonzia, branca dell’odontoiatria che si occupa della cura dei denti da latte. I bambini sono spesso considerati pazienti difficili per i quali la terapia può rappresentare fonte di ansia e preoccupazioni.
            Il nostro Centro ha sviluppato negli anni un approccio terapeutico delicato e molto orientato a sostenere psicologicamente il bambino nel corso della terapia.
            Le cure restaurative e conservative, svolte allo scopo di preservare la dentizione fino alla permuta e di mantenere lo spazio per i denti permanenti, vengono praticate con estrema pazienza, rassicurando e rispettando i piccoli pazienti.'],




            ['img'=>'/img/Sedazione Cosciente.jpg','title'=>'Sedazione Cosciente','description'=>'La maggior parte dei pazienti quando va dal dentista ha paura, o comunque prova un senso di ansia, angoscia, apprensione, agitazione. Molte azioni fatte senza anestesia sono dolorose o molto fastidiose come: l’ablazione del tartaro, la presa delle impronte, l’uso di matrici, aria ed acqua su denti sensibili, esplorazione di carie o tasche parodontali con specilli.','indicazioni'=>'L’analgesia sedativa si applica nel corso di tutte le pratiche odontoiatriche abituali: igiene (detartrasi e scaling), protesi, conservativa, endodonzia, estrazioni, chirurgia parodontale e impianti.
            È inoltre consigliata a tutte le persone ansiose, che hanno vissuto un’esperienza trumatica dal dentista e a tutti i bambini.
            È un metodo assolutamente sicuro e che dura solo il tempo strettamente necessario e quindi permette ad ognuno di tornare alle propria attività senza nessun fastidio o effetto collaterale.']
        ];
        foreach($cards as $card){
            if ($card['title']== $title){
                    return view('dettaglio',['card'=>$card]);
            }
        }
    
    }

    public function submit(Request $request){
        
        $user = $request->input('user');
        $message=$request->input('message');

        $email=$request->input('email');

        $contact = compact('user', 'message');

        Mail::to($email)->send(new ContactMail($contact));

        return redirect(route('grazie'))->with('message','la tua richiesta è stata inoltrata,grazie!');
    }

    public function grazie () {
        return view('grazie');
    }

}
