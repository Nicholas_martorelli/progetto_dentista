<x-layout>

    <x-navbar/>
    
    
    
    
    <div class="container  m-top-100">
        <div class="card border-0 shadow my-5">
          <div class="card-body p-5">
         @if (session('message'))
            <div class="alert alert-success">
              {{ session('message')}}
            </div>
          @endif
             <a href="{{route('home')}}" class=" text-center button-service rounded-pill btn-outline tc-main search px-2"> Torna alla Home</a>
          </div>
        </div>
      </div>
    
      <div style="height: 300px"></div>
    
      <x-fouter/>
    
    </x-layout>