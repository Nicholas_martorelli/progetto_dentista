<nav class="navbar  navbar-expand-lg  bc-nav z-index">
    <div class="container-fluid">
      <a class="navbar-brand tc-sec mx-5 fw-bold" href="{{route('home')}}">Doc.Tooth</a>
      <button class="navbar-toggler " type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fas fa-tooth tc-sec"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link tc-sec fw-bold" aria-current="page" href="{{route('home')}}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link tc-sec fw-bold" href="{{route('servizi')}}">Servizi</a>
          </li>
          <li class="nav-item">
            <a class="nav-link tc-sec fw-bold" href="{{route('chi-siamo')}}">Chi Siamo</a>
          </li>
        </ul>
        <form class="d-flex me-5">
          <input class="form-control me-2 rounded-pill" type="search" placeholder="Cerca..." aria-label="Search">
          <button class=" btn-outline search fw-bold rounded-pill" type="submit">Search</button>
        </form>
      </div>
    </div>
  </nav>