<footer>
    <div class="container-fluid text-white mt-5 pt-3 fouter">
        <h2 class="text-center">Doc.Tooth</h2>
        <div class="row">
            <div class="col-6 fw-lighter p-3">
              <p>Note Legali   Privacy</p>
              <p>2004-2021 Tutti i diritti sono riservati.</p>
             </div>
             <div class="col-6 tc-main text-center pt-4 ">
                <i class="fab fa-twitter mx-3 icon-fouter"></i>
                <i class="fab fa-facebook-square mx-3 icon-fouter"></i>
                <i class="fab fa-google-plus-g mx-3 icon-fouter"></i>
                <i class="fab fa-instagram mx-3 icon-fouter"></i>
            </div>
         </div>
    </div>

</footer>