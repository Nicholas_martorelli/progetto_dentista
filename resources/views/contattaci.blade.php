<x-layout>

<x-navbar/>




<div class="container  m-top-100">
    <div class="card border-0 shadow my-5">
      <div class="card-body p-5">
        <h1 class="text-center fw-bold tc-sec mb-5">
            Contattaci!
        </h1>
        <div >
            <form method="POST" action="{{route('contattaci.submit')}}">
                @csrf
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Nome Utente</label>
                  <input name="user"type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Email</label>
                    <input name="email"type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                  </div>
                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label ">Descrizione</label>
                  <textarea name="message" id="exampleInputPassword1" cols="25" rows="7"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Invia</button>
            </form>
        </div>
      </div>
    </div>
  </div>


  <x-fouter/>

</x-layout>