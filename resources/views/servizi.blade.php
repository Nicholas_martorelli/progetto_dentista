<x-layout>

<x-navbar/>



<div class="container m-top-100">
    <div class="card border-0 shadow my-5">
      <div class="card-body card-color p-5">
        <h1 class="text-center tc-sec fw-bold fst-italic" data-aos="fade-right"data-aos-duration="3000" >I Nostri Servizi</h1>
            <div class="container my-5">
                <div class="row">
                    @foreach ($cards as $card)
                    <div class="col-12 col-md-6 col-lg-4 my-3 data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="2000">
                        <div class="card bg-dark ">
                            <img src="{{$card['img']}}" class="img-fluid" alt="...">
                            <div class="card-img-overlay">
                                <h2 class="tc-black fst-italic text-center">{{$card['title']}}</h2>
                                <a href="{{route('servizi.dettaglio',['title'=>$card['title']])}}" class="button-service rounded-pill btn-outline tc-main search px-2 position-absolute bottom-0 start-50 translate-middle">Clicca qui per info...</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
      </div>
    </div>
  </div>








<x-fouter/>



</x-layout>