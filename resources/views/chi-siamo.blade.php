<x-layout>
    <x-navbar/>

    <div class="container m-top-100">
        <div class="card border-0 shadow my-5">
          <div class="card-body card-color p-5">
            <h1 class="text-center tc-sec fw-bold fst-italic">I Nostri Collaboratori</h1>
                <div class="container my-5">
                    <div class="row">
                        @foreach ($cards as $card)
                        <div class="col-12 col-lg-6" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="2000">
                            <div class="card mb-3 px-3 py-3 my-4"  style="max-width: 540px;"> 
                                <div class="row g-0">
                                  <div class="col-md-4 align-items-center d-flex">
                                    <img class="img-fluid rounded-pill"src="{{$card['img']}}" alt="...">
                                  </div>
                                  <div class="col-md-8">
                                    <div class="card-body">
                                      <h5 class="card-title tc-sec">{{$card['name']}} {{$card['surname']}}</h5>
                                      <p class="card-text">{{$card['description']}}</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                        </div>
                        @endforeach
                    </div>
                </div>
          </div>
        </div>
    </div>

       <x-fouter/>
 </x-layout>

