<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Doc.Tooth</title>
    {{-- Mio Stile --}}
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
        {{--boostrap style--}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    {{--fontawesome--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    {{--google font--}}
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    {{--css animazioni--}}
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
</head>
<body>

  <x-navbar></x-navbar>


     <div class="container m-top-100">
    
        <h1 class="text-center tc-sec fw-bold fst-italic" data-aos="fade-right"data-aos-duration="3000" >{{$card['title']}}</h1>
            <div class="container my-5">
                <div class="row">
                    <div class="col-12 col-md-6 text-center ">
                        <img class="img-fluid mb-3" data-aos="zoom-in-down" data-aos-duration="1500" src="{{$card['img']}}" alt="">
                        <a href="{{route('servizi')}}" class=" button-service rounded-pill btn-outline tc-main search px-2 ">Torna ai Servizi</a>
                    </div>
                    <div class="col-12 col-md-6">
                        <p class="mb-3" data-aos="zoom-out-right" data-aos-duration="1500">{{$card['description']}}</p>
                        <h2 class="tc-main my-3"  data-aos="flip-left"data-aos-easing="ease-out-cubic" data-aos-duration="1000">Indicazioni</h2>
                        <p class="mt-3" data-aos="fade-up"data-aos-anchor-placement="center-bottom" data-aos-duration="1000">{{$card['indicazioni']}}</p>
                    </div>
                </div>
            </div>
 
    </div>
    












  <x-fouter/>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
  {{--animazioni script--}}
  
  
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  
  <script>
      AOS.init();
  </script>
</body>
</html>
