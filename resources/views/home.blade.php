<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Doc.Tooth</title>
    {{-- Mio Stile --}}
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
        {{--boostrap style--}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    {{--fontawesome--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    {{--google font--}}
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    {{--css animazioni--}}
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
</head>
<body>

  <x-navbar></x-navbar>





  <header class="masthead">
      <div class="container h-100">
        <div class="row h-100 align-items-center">
          <div class="col-12 text-center">
           <h1 class="tc-sec fw-bold ts">Doc.Tooth</h1>
           <h3>Il Mago Dei Denti</h3>
          </div>
        </div>
      </div>
    </header>



    <div class="container my-5 mb-5">
      <div class="row">
        <div class="col-12">
          <h2 class="tc-sec fw-bold text-center my-5">Il Nostro Studio</h2>
        </div>
        <div class="col-12 col-md-7 text-center">
            <p class="my-5 tc-black">Un sorriso vale più di mille parole, soprattutto quando questo sorriso risulta sano e composto da denti controllati, bianchi e senza problematiche. Il centro medico e odontoiatrico si occupa del benessere estetico e della salute del tuo cavo orale, grazie al supporto di un team di professionisti e tecnici sempre al passo con le ultime metodologie del settore.</p>
            <p class="my-5 tc-black"> Presso il centro medico troverai uno staff cordiale, empatico e a tua totale disposizione, in un ambiente polifunzionale dove la salute dei denti è solo uno dei tanti servizi proposti. Per qualsiasi esigenza medica, presso il centro Vitality troverai il professionista adatto alle tue necessità. Non solo dentisti quindi, ma molto di più: scopri tutte le categorie d’interesse sfogliando il sito web.</p>
            <a href="{{route('contattaci')}}" class="button-service rounded-pill btn-outline tc-main search px-2 ">Contattaci!</a>
        </div>
        <div class="col-12 col-md-5">
          <img class="img-fluid"src="/img/dente home.webp" alt="dente">
        </div>
      </div>
    </div>



    <div class="container mt-5">
      <div class="row">
        <div class="col-12 col-md-7 ">
          <img src="/img/Area-bambini.jpg" alt="area gioco" class="img-fluid">
        </div>
        <div class="col-12 col-md-5">
          <p class="my-5 tc-black text-center">
            I bambini possono divertirsi con vari giochi da parete, incluso il nostro touchscreen da 17 pollici con cui i bambini possono giocare con giochi interattivi. L'istruzione è una priorità assoluta per IKC e questo si riflette proprio nel software creato. I bambini imparano ogni cosa su animali, numeri e lettere. I giochi da parete e lo speciale tavolo di perline sfidano i bambini a stimolare la coordinazione occhio-mano in modo ludico. Un concept di gioco di IKC ha un effetto positivo sui bambini! Per i bambini il disegno a muro è riconoscibile. Diversi animali della giungla sono stati inseriti sui pannelli in forex posti sul muro rendendo quindi un’immagine completa.
          </p>
        </div>
      </div>
    </div>








   



   


   
    <x-fouter/>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    {{--animazioni script--}}
    
    
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    
    <script>
        AOS.init();
    </script>
</body>
</html>

